# benshmark ([ishortn.ink/benshmark](http://ishortn.ink/benshmark))

Comparação, por simples repetição, da velocidade de scripts em BASh.

## TODO
- [ ] testar a existência  do locale `pt_BR.UTF-8` na versão 2 e do locale `en_US.UTF-8` na versão 3
- [x] implementar a versão 5 independente do `bc` em locale `C`

## Características
- os procedimentos cujas velocidades de execução serão comparadas devem ser armazenados em funções `bash`;
- os nomes das funções são irrelevantes. Ou todos serão listados como parâmetros, ou, se houver algum padrão que permita o uso de expansão de chaves, a string que os representa poderá ser usada;
- uma mensagem de indicação de uso é apresentada caso nenhuma função seja fornecida ou o primeiro parâmetro não seja um inteiro > 0;
- as funções passadas como parâmetro mas eventualmente não definidas são explicitamente identificadas;
- a comparação é feita, por padrão, em "números de vezes", mas se o último parâmetro for o caractere opcional %, será em porcentagem;
- os valores de tempo e proporção resultantes são apresentados em notação brasileira (ponto na separação de milhar e vírgula decimal) sem depender do locale `pt_BR.UTF-8`;
- se a solução mais rápida tiver uma medida de tempo 0, a divisão por zero será levada em consideração.
- em um script, a menos que seja executado por `source`, o uso de `alias` exigirá habilitar a expansão de aliases: `shopt -s expand_aliases`

## Versões
- [versão 1](v1.sh) - apenas executa as repetições e retorna os tempos na ordem de execução.
- [versão 2](v2.sh) - também processa os tempos, identifica a solução mais rápida e lista todas em ordem crescente de tempo com a proporção de cada um em relação à mais rápida (calculada com `bc`).
                      Depende da existência do locale `pt_BR.UTF-8` para apresentar os valores em notação brasileira.
- [versão 3](v3.sh) - assume locale `en_US.UTF-8` e formata os valores em notação brasileira por manipulação de strings.
- [versão 4](v4.sh) - assume locale `C` e formata os valores em notação brasileira por manipulação de strings.
- [versão 5](v5.sh) - `[estável]` assume locale `C`, formata os valores por manipulação de strings e calcula a proporção com 3 casas decimais sem recorrer ao `bc`, usando apenas operações matemáticas do bash.
                      Variáveis mais inteligíveis.
- [versão 6](v6.sh) - `[desenvolvimento]` inclui limpeza do cache de RAM em cada laço de loop. Exige autenticação `sudo`. Aumenta o tempo de execução de cada laço.

## Uso
```shell
source <(curl -ksL ishortn.ink/benshmark-v5)
# Criado alias bm para a função benshmark-v5
#
# Em um script, a menos que seja executado por source, o uso de alias exige habilitar a expansão de aliases:
# shopt -s expand_aliases

# ex com operação aritmética
s1(){ expr 2 + 2; }
s2(){ echo $[2+2]; }
s3(){ echo $((2+2)); }
s4(){ bc <<< '2+2'; }

# ex com criação de arquivo
c1(){ touch /tmp/x; }
c2(){ > /tmp/x; }
c3(){ cp /dev/null /tmp/x; }

# verificação do resultado de cada solução
eval s{1..4}\;
# 4
# 4
# 4
# 4

bm
bm 2000
bm 2000 %
bm s{1..4}
# Uso: benshmark <repetições> <função> <função> [...] [%]

bm 2000 s{1..6}
# As funções (s5 s6) não estão definidas.

bm 2000 s{1..5}
# A função (s5) não está definida.

bm 2000 s{1..4}
# s1  00:02,159
# s2  00:00,014
# s3  00:00,016
# s4  00:03,379
#
# 1º  s2
# 2º  s3/s2       0,142 vez mais lenta
# 3º  s1/s2     153,214 vezes mais lenta
# 4º  s4/s2     240,357 vezes mais lenta

bm 2000 s{1..4} %
# s1  00:04,309
# s2  00:00,009
# s3  00:00,010
# s4  00:03,413
#
# 1º  s2
# 2º  s3/s2      11,111 % mais lenta
# 3º  s4/s2  37.822,222 % mais lenta
# 4º  s1/s2  47.777,777 % mais lenta

bm 10 s{1..4} %
# s1  00:00,023
# s2  00:00,000
# s3  00:00,001
# s4  00:00,030
#
# 1º  s2
# 2º  s3/s2  ∞
# 3º  s1/s2  ∞
# 4º  s4/s2  ∞
```

☐
