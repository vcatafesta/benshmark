# vim: ft=sh

# Arkanon <arkanon@lsd.org.br>
# 2022/06/29 (Wed) 10:12:22 -03



benshmark-v2()
{
  local LC_ALL= LC_NUMERIC=pt_BR.UTF-8 &> /dev/null
  local TIMEFORMAT=%lR
  local L=$1 mult unit undef p1 p2=ão p3=á
  shift
  [[ ${@: -1} == % ]] &&
  {
    mult=100 unit=%%
    set -- ${@:1:$(($#-1))}
  } || mult=1 unit=vezes
  [[ $* && $L =~ ^[0-9]+$ ]] ||
  {
    echo "Uso: $FUNCNAME <repetições> <função> <função> [...] [%]" >&2
    return 255
  }
  declare -F $* &> /dev/null ||
  {
    undef=( $(grep -vf <(declare -F $*) <(printf '%s\n' $*)) )
    (( ${#undef[*]} > 1 )) && p1=s p2=ões p3=ão
    echo "A$p1 funç$p2 (${undef[*]}) não est$p3 definida$p1." >&2
    return 1
  }
  echo
  for s
  {
    echo -n "$s  " >&2
    IFS=ms read M S <<< $( { time for ((i=0;i<L;i++)); { $s; } &> /dev/null; } 2>&1 )
    printf "%02i:%06.3f\n" $M ${S/./,} >&2
    echo "$M*60+${S/,/.} $s"
  } | sort -V | {
    read bt bs
    echo -e "\n$((n=1))º  $bs"
    while read t s
    do
      echo -n "$((++n))º  $s/$bs  "
      r=$(
           bc <<< "
            l=$t
            r=$bt
            m=$mult
            scale=3
            if (r>0) m*(l-r)/r
         ")
      [[ $r ]] && printf "%'10.3f $unit mais lenta\n" ${r/./,} || echo ∞
    done
  }
  echo
}



# EOF
