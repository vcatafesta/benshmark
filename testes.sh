# vim: ft=sh

# Arkanon <arkanon@lsd.org.br>
# 2023/04/15 (Sat) 23:44:02 -03
# 2022/07/20 (Wed) 04:50:53 -03



dbg(){ echo $1=${!1} >&2; }



v5-dev()
{

  # s1  00:03,848
  # s2  00:00,027
  # s3  00:00,015
  # s4  00:03,276
  #
  # 1º  s3
  # 2º  s2/s3       0,800 vezes mais lenta
  # 3º  s4/s3     217,400 vezes mais lenta
  # 4º  s1/s3     255,533 vezes mais lenta

  echo
 fator=100 unidade=%
#fator=1   unidade=vez
  precisao=3
  dbg fator
  dbg unidade
  dbg precisao
  for entrada in 0m3.848s 0m0.027s 0m0.015s 0m3.276s
  {
    echo >&2
    IFS=ms read minutos segundos <<< $entrada
    dbg entrada
    dbg minutos
    dbg segundos
    LC_ALL=C printf -v tempo "%02i:%06.3f" $minutos $segundos
    dbg tempo
    echo "$minutos*60*10**precisao+10#${segundos/./}*10**(precisao-3)" | tee /dev/stderr
  } | sort -V |
  {
    read melhortempo
    echo -e "\n$((n=1))º"
    melhortempo=$((10#$melhortempo))
    dbg melhortempo
    echo
    while read tempo
    do
      echo -n "$((++n))º  "
      (( melhortempo > 0 )) &&
      {
            tempo=$((10#$tempo))
        dbg tempo
        proporcao=$((10**precisao*fator*(tempo-melhortempo)/melhortempo))
        dbg proporcao
        (( proporcao > 0 )) &&
        {
          inteiro=${proporcao::-$precisao}
          dbg inteiro
          decimal=${proporcao: -$precisao:$precisao}
          dbg decimal
          (( ${#inteiro} > 10**precisao )) && [[ $unidade == vez ]] && plural=es
          (( ${#inteiro} > 3 )) &&
          milhar=${inteiro::-$precisao}. centena=${inteiro: -$precisao:$precisao} ||
          { milhar=; (( ${#inteiro} > 0 )) && centena=$inteiro || centena=; }
          dbg milhar
          dbg centena
          echo $milhar${centena:-0},$decimal $unidade$plural mais lenta
          echo
        } || echo =
      } || echo ∞
    done
  }

}



squashfs()
{

  return

  sudo -i

  iso=ubuntu-22.04-desktop-amd64.iso

  7z l $iso | grep squashfs
  # 2022-04-19 07:17:07 .....   2502324224   2502324224  casper/filesystem.squashfs
  # 2022-04-19 07:19:20 .....          833          833  casper/filesystem.squashfs.gpg

  sfs=filesystem.squashfs

  time 7z e $iso casper/$sfs
  # ...
  # 1m50,794s

  du -h $sfs
  # 2,4G    filesystem.squashfs

  unsquashfs -ll $sfs | sed -r '1,3d; /s*-root\/dev/d' | wc -l
  # 220884

  time unsquashfs -ll $sfs &> /dev/null
  # 0m2,317s

  dir=/mnt/0

  sudo mount -o loop -t squashfs $sfs $dir

  df -hT $dir
  # Sist. Arq.     Tipo      Tam. Usado Disp. Uso% Montado em
  # /dev/loop0     squashfs  2,4G  2,4G     0 100% /mnt/0

  eval s{1..4}\;
  # 5815475187      /mnt/0
  # 6849070764
  # 6849070509
  # 6849070784

  source v5.sh
  alias bm=benshmark-v5

  s1(){ du -bs $dir; }
  s2(){ echo $(( $(find $dir -printf %s+)0 )); }
  s3(){ echo $(( $( unsquashfs -ll $sfs | sed -r ' 1,3d; /s*-root\/dev/d; s/^([^ ]+ ){2}//; s/^ *([^ ]+) .*/\1+/; $s/\+//; ' ) )); }
  s4(){ unsquashfs -ll $sfs | awk '{m+=$3} END {print m}'; }

  bm 1 s{1..4}
  # s1  00:00,402
  # s2  00:00,404
  # s3  00:02,275
  # s4  00:02,287
  #
  # 1º  s1
  # 2º  s2/s1       0,004 vezes mais lenta
  # 3º  s3/s1       4,659 vezes mais lenta
  # 4º  s4/s1       4,689 vezes mais lenta

  umount $dir
# rm $sfs
# exit

}



# EOF
